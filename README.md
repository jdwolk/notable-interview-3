# Notable Interview 3

## Requirements

* nodejs (tested w/ v16.10.0)
* yarn

## Install

```
$ yarn install
```

## Run

```
$ node match.js
```
