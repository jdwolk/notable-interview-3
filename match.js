const R = require("ramda");

// TODO: Finish implementing a data encoding
// that will obviate the need for parsing age strings
const AnyAge = () => ({
  type: "any",
});

const AgeRange = ({ min, max }) => ({
  type: "range",
  min,
  max,
});

const Exactly = (value) => ({
  type: "exactly",
  value,
});
// TODO: end

// reasonMatcher :: String -> Boolean
const reasonMatcher = R.curry((reason, { reason: scenarioReason }) => {
  return reason === scenarioReason;
});

// ageMatcher :: String -> Boolean
const ageMatcher = R.curry((age, { age: scenarioAge }) => {
  // fingerprint age format:
  // 36
  // ----
  // Scenario age formats:
  // any
  // 41+
  // 41-
  // between 20, 40
  // TODO: dispatch on Age data types ^^^ to
  // determine whether they match age
  return true;
});

// insuranceMatcher :: String -> Boolean
const insuranceMatcher = R.curry(
  (insurance, { insurance: scenarioInsurance }) => {
    // TODO
    return true;
  }
);

// getMatchersFrom :: Fingerprint -> [MatcherFn]
const getMatchersFrom = ({ reason, age, insurance }) => {
  return [reasonMatcher(reason), ageMatcher(age), insuranceMatcher(insurance)];
};

const match = ({ fingerprint, scenarios }) => {
  const matchersForFingerprint = getMatchersFrom(fingerprint);

  return R.filter(R.allPass(matchersForFingerprint), scenarios);
};

const ordersFrom = (matchingScenarios) => {
  // TODO de-dupe the orders
  return R.map(R.prop("orders"), matchingScenarios);
};

const run = ({ fingerprint, scenarios }) => {
  return ordersFrom(match({ fingerprint, scenarios }));
};

const fingerprint = {
  reason: "annual_exam",
  age: "36",
  insurance: "medicare",
};

const scenarios = [
  {
    name: "annual_exam_1",
    reason: "annual_exam",
    age: "between 20, 40",
    insurance: "any",
    orders: ["folic acid", "complete blood count"],
  },
  {
    name: "annual_exam_2_medicare",
    reason: "annual_exam",
    age: "41+",
    insurance: "medicare",
    orders: ["complete blood count", "cancer screen"],
  },
  {
    name: "annual_exam_2",
    reason: "annual_exam",
    age: "41+",
    insurance: "!medicare",
    orders: ["complete blood count", "a1c", "cancer screen"],
  },
  {
    name: "postop_hip",
    reason: "postop_hip",
    age: "any",
    insurance: "any",
    orders: ["xray, hip", "stitch removal"],
  },
];

//match({ fingerprint, scenarios });

const testFingerprint1 = {
  reason: "annual_exam",
  age: "36",
  insurance: "medicare",
};

const testScenarios1 = [
  {
    name: "doesnt match",
    reason: "doesnt match reason",
    age: Exactly("40"),
    insurance: "medicare",
    orders: ["folic acid"],
  },
  {
    name: "annual_exam name",
    reason: "annual_exam",
    age: Exactly("36"),
    insurance: "medicare",
    orders: ["complete blood count", "cancer screen"],
  },
  {
    name: "annual_exam name2",
    reason: "annual_exam",
    age: AgeRange({ min: "55", max: "55" }),
    insurance: "medicare",
    orders: ["complete blood count", "cancer screen"],
  },
  {
    name: "annual_exam123 name",
    reason: "annual_exam123",
    age: AnyAge(),
    insurance: "medicare",
    orders: ["cancer screen"],
  },
];

const result1 = match({
  fingerprint: testFingerprint1,
  scenarios: testScenarios1,
});
console.log(result1);
