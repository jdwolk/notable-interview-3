### Setup

For this portion of the interview, please make sure you have a clean-slate coding environment set up in your preferred language. The code/program written during the interview needs to compile and run.

For example, if your coding language of preference is Javascript, you can simply use a blank template on jsbin (https://jsbin.com/?js,console).

### Challenge

Doctors perform a lot of repetitive, routine appointments where most of what happens during the visit is predetermined and can be completed in advance.  For example, every appointment looks very similar, with only slight differences based on patient age, visit reason and insurance.

In order to automate this repetitive work, we want to build a program that, given a set of information about an encounter (its "fingerprint") and a set of scenarios, returns all the orders from scenarios that matches the fingerprint.

### Example Inputs

Fingerprint:
    reason = annual_exam
    age = 36
    insurance = medicare

Scenarios:
    name = annual_exam_1
    reason = annual_exam
    age = between 20, 40
    insurance = any
    orders = ['folic acid', 'complete blood count']

    name = annual_exam_2_medicare
    reason = annual_exam
    age = 41+
    insurance = medicare
    orders = ['complete blood count', 'cancer screen']

    name = annual_exam_2
    reason = annual_exam
    age = 41+
    insurance = !medicare
    orders = ['complete blood count', 'a1c', 'cancer screen']

    name = postop_hip
    reason = postop_hip
    age = any
    insurance = any
    orders = ['xray, hip', 'stitch removal']
